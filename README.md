# API empresa-backend-luiz-junior com Node.js

### Pré-requisitos

- **Node.js** versão 8 ou superior;

### Instalação e Execução

1. Faça o clone do repositório e no terminal navegue até a pasta;
2. Instale as dependências do projeto com `npm install`;
3. Configurar os dados do BD no arquivo src/config/config.js e rodar o script do BD src/sql/db_test.sql, 
4. Rode o servidor de desenvolvimento com `npm run start`;
6. O *endpoint* do serviço estará disponível em http://localhost:3000 .


### Sugestão

Utilize o Postman para testar suas chamadas. [https://www.getpostman.com/](https://www.getpostman.com/).
