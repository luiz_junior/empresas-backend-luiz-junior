module.exports = {
    database: "test",
    username: "postgres",
    password: "postgres",
    params: {
        dialect: "postgres",
        define: {
            underscored: false
        }
    },
    jwt: {
        secret: 't0p-S3cr3t',
        session: {session: false}
    }
};
