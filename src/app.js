const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const config = require("./config/config");
const datasource = require("./config/datasource");

const indexRouter =  require("./routes/index");
const usersRouter = require("./routes/users");
const enterpriseTypesRouter = require("./routes/enterpriseTypes");
const enterprisesRouter = require("./routes/enterprises");
const authRouter = require('./routes/auth');

const authorization = require('./auth');

const app = express();
app.use(cors());

const port = 3000;
app.set('port', port);

app.config = config;
app.datasource = datasource(app);

app.use(bodyParser.json({
    limit:'5mb'
}));

const auth = authorization(app);
app.use(auth.initialize());
app.auth = auth;

indexRouter(app);
usersRouter(app);
enterpriseTypesRouter(app);
enterprisesRouter(app);
authRouter(app);

module.exports = app;
