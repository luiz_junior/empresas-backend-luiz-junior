module.exports = (sequelize, DataType) => {

    const Enterprise = sequelize.define('enterprise', {
        id: {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        enterprise_name: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        description: {
            type: DataType.TEXT,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        email_enterprise: {
            type: DataType.STRING,
            allowNull: true
        },
        facebook: {
            type: DataType.STRING,
            allowNull: true
        },
        twitter: {
            type: DataType.STRING,
            allowNull: true
        },
        linkedin: {
            type: DataType.STRING,
            allowNull: true
        },
        phone: {
            type: DataType.STRING,
            allowNull: true
        },
        own_enterprise: {
            type: DataType.BOOLEAN,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        photo: {
            type: DataType.STRING,
            allowNull: true
        },
        value: {
            type: DataType.FLOAT,
            allowNull: true
        },
        shares: {
            type: DataType.INTEGER,
            allowNull: true
        },
        share_price: {
            type: DataType.INTEGER,
            allowNull: true
        },
        own_shares: {
            type: DataType.INTEGER,
            allowNull: true
        },
        city: {
            type: DataType.STRING,
            allowNull: true
        },
        country: {
            type: DataType.STRING,
            allowNull: true
        }

    });

    const enterpriseType = require('./EnterpriseType');

    console.log('enterpriseType', enterpriseType(sequelize, DataType))
    Enterprise.belongsTo(enterpriseType(sequelize, DataType));

    return Enterprise;
};
