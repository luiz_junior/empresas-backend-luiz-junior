module.exports = (sequelize, DataType) => {

    return sequelize.define('enterprise_type', {
        id: {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        enterprise_type_name: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
    });
};
