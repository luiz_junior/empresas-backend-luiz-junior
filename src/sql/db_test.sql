--
-- PostgreSQL database dump
--

-- Dumped from database version 10.8
-- Dumped by pg_dump version 11.2

-- Started on 2020-01-09 07:30:01 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE test;
--
-- TOC entry 3141 (class 1262 OID 25679)
-- Name: test; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE test WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE test OWNER TO postgres;

\connect test

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 3142 (class 0 OID 0)
-- Dependencies: 3141
-- Name: DATABASE test; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON DATABASE test FROM postgres;
GRANT CREATE,CONNECT ON DATABASE test TO postgres;
GRANT TEMPORARY ON DATABASE test TO postgres WITH GRANT OPTION;


-- Completed on 2020-01-09 07:30:01 -03

--
-- PostgreSQL database dump complete
--

