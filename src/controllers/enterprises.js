const HttpStatus = require("http-status");
const Sequelize = require('sequelize');

const Op = Sequelize.Op

const defaultResponse = (data, status = HttpStatus.OK) => ({
    data,
    status
});

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => defaultResponse({
    error: message,
    status
}, status);

class EnterprisesController {

    constructor(modelEnterprise, modelEnterpriseType) {

        this.Enterprises = modelEnterprise;
        this.EnterpriseType = modelEnterpriseType;

    }

    getAll(params) {
        let where = {};

        if (params.enterpriseTypeId && params.enterprise_name) {
            const search = `%${params.enterprise_name}%`;
            where = { include: [{model: this.EnterpriseType}], where: { enterpriseTypeId: params.enterpriseTypeId, enterprise_name: { [Op.like]: search } } }
        }

        return this.Enterprises
                .findAll( where )
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));

    }

    getById(params) {
        return this.Enterprises
                .findOne({where: params})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));

    }

    async getType(data) {
        const response = {
            enterpriseType: {
                id: null,
                enterprise_type_name: null
            }
        };

        if(data.id) {

            const id = data.id;

            try {

                const result = await this.EnterpriseType.findOne({id});

                const enterpriseType = await result;
                if(enterpriseType) {

                    response.enterpriseType.id = enterpriseType.id;
                    response.enterpriseType.enterprise_type_name = enterpriseType.enterprise_type_name;

                    return response;

                } // end if

            } catch (e) {

                console.error(e);

            }


        } // end if

        return response;

    }



}

module.exports = EnterprisesController;
