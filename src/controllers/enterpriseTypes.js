const HttpStatus = require("http-status");

const defaultResponse = (data, status = HttpStatus.OK) => ({
    data,
    status
});

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => defaultResponse({
    error: message,
    status
}, status);

class EnterpriseTypesController {

    constructor(modelEnterpriseType) {

        this.EnterpriseType = modelEnterpriseType;

    }

    getAll(params) {
        let where = {};

        if (params.id) {
            where = { where: { id: params.id } }
        }
        return this.EnterpriseType
                .findAll( where )
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));

    }

    getById(params) {
        return this.EnterpriseType
                .findOne({where: params})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));

    }


}

module.exports = EnterpriseTypesController;
