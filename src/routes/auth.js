const HttpStatus = require('http-status');
const jwt = require('jsonwebtoken');
const UsersController = require('../controllers/users');

module.exports = (app) => {
    const usersController = new UsersController(app.datasource.models.users);

    app.route('/api/v1/users/auth/sign_in')
        .post( async (req, res) => {

            try {
                const response = await usersController.signin(req.body);
                const login = response.login;

                if(login.id && login.isValid) {

                    const payload = {
                        id: login.id,
                    };

                    const token = jwt.sign(payload, app.config.jwt.secret, {expiresIn: '1h'})
                    const tk = String(token).split('.')
                    res.header( 'access-token', token );
                    res.header( 'token-type', 'Bearer' );
                    res.header( 'client', tk[1] );
                    res.header( 'uid', login.uid );
                    res.status(200).send({
                        investor: {
                            id: login.id,
                            email: login.uid
                        },
                        enterprise: null,
                        success: login.isValid
                    });
                } else {

                    res.sendStatus(HttpStatus.UNAUTHORIZED);
                }


            } catch (e) {

                console.error(e);
                res.sendStatus(HttpStatus.UNAUTHORIZED);

            }

        });

};
