const EnterpriseTypesController = require('../controllers/enterpriseTypes');

module.exports = (app) => {

  const enterpriseTypesController = new EnterpriseTypesController(app.datasource.models.enterprise_type);

  app.route('/api/v1/enterprise_type')
    .get((req, res) => {
      const params = {
        id: req.query.id,
        email: req.query.email
      }
      console.log('query', req.query);

      enterpriseTypesController
        .getAll(params)
        .then(rs => {
          res.json(rs.data);
        })
        .catch(error => {
          console.error(error.message);
          res.status(error.status);
        });

    });
  app.route('/api/v1/enterprise_type/:id')
    .get((req, res) => {

      console.log('params', req.params);
      enterpriseTypesController
        .getById(req.params)
        .then(rs => {
          res.json(rs.data);
        })
        .catch(error => {
          console.error(error.message);
          res.status(error.status);
        });
    })

};
