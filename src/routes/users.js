const UsersController = require('../controllers/users');

module.exports = (app) => {

    const usersController = new UsersController(app.datasource.models.users);

    app.route('/api/v1/users')
        .all(app.auth.authenticate())
        .get((req, res) => {
            const params = {
              id: req.query.id,
              email: req.query.email
            }
            console.log('query', req.query);

            usersController
                .getAll(params)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });

        });

  app.route('/api/v1/users/sign_up')
      .post((req, res) => {

            usersController
                .create(req.body)
                .then(rs => {

                    res.json(rs.data);
                    res.status(rs.status);

                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

        app.route('/api/v1/users/:id')
            .all(app.auth.authenticate())
            .get((req, res) => {

                console.log('params', req.params);
                usersController
                    .getById(req.params)
                    .then(rs => {
                        res.json(rs.data);
                    })
                    .catch(error => {
                        console.error(error.message);
                        res.status(error.status);
                    });
            })
            .put((req, res) => {

                usersController
                    .update(req.body, req.params)
                    .then(rs => {
                        res.json(rs.data);

                    })
                    .catch(error => {
                        console.error(error.message);
                        res.status(error.status);
                    });
            })
            .delete((req, res) => {
                usersController
                    .delete(req.params)
                    .then(rs => {
                        res.json(rs.data);
                        res.status(rs.status);
                    })
                    .catch(error => {
                        console.error(error.message);
                        res.status(error.status);
                    });
            });

};
