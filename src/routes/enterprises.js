const EnterprisesController = require('../controllers/enterprises');
const HttpStatus = require('http-status');

module.exports = (app) => {

  const enterprisesController = new EnterprisesController(app.datasource.models.enterprise, app.datasource.models.enterprise_type);

  app.route('/api/v1/enterprises')
    .all(app.auth.authenticate())
    .get( async (req, res) => {
      const params = {
        enterpriseTypeId: req.query.enterprise_types,
        enterprise_name: req.query.name
      }

      try {
          const responseEnt = await enterprisesController.getAll({params});
          const enterprise = JSON.parse(JSON.stringify(responseEnt.data))
          for (const value of enterprise) {
            if (value.enterpriseTypeId) {
              const response = await enterprisesController.getType({id: value.enterpriseTypeId});
              value.enterprise_type = response.enterpriseType;
              delete value.enterpriseTypeId;
              const data = {
                enterprise: value,
                success: true
              }

              res.json(data)
            }
          }
        } catch (e) {

        console.error(e);
        res.sendStatus(HttpStatus.UNAUTHORIZED);

      }

    });
  app.route('/api/v1/enterprises/:id')
    .all(app.auth.authenticate())
    .get(async (req, res) => {

        try {
          const responseEnt = await enterprisesController.getById(req.params);
          const enterprise = JSON.parse(JSON.stringify(responseEnt.data))
          if (enterprise && enterprise.enterpriseTypeId) {
            const response = await enterprisesController.getType({id: enterprise.enterpriseTypeId});
            enterprise.enterprise_type = response.enterpriseType;
            delete enterprise.enterpriseTypeId;
            const data = {
              enterprise: enterprise,
              success: true
            }

            res.json(data)
          }
        } catch (e) {

          console.error(e);
          res.sendStatus(HttpStatus.UNAUTHORIZED);

        }
    })

};
